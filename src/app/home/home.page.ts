import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  public paginas = [
    {
      direccion: '/pagina1',
      titulo: 'Juan Perez',
      imagen: 'assets/avatares/avatar11.png'
    },
    {
      direccion: '/pagina2',
      titulo: 'Pancho Lopez',
      imagen: 'assets/avatares/avatar22.png'
    },
    {
      direccion: '/pagina3',
      titulo: 'Lupito Gimenez',
      imagen: 'assets/avatares/avatar33.png'
    },
    {
      direccion: '/pagina4',
      titulo: 'Fulano de Tal',
      imagen: 'assets/avatares/avatar44.png'
    },
    {
      direccion: '/pagina5',
      titulo: 'Pablo Garcia',
      imagen: 'assets/avatares/avatar55.png'
    }
  ]
  constructor() {}

}
